package com.barry.customerservice.mapper;


import com.barry.customerservice.dto.request.CustomerRequest;
import com.barry.customerservice.dto.response.CustomerResponse;
import com.barry.customerservice.model.Customer;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface CustomerMapper {

    CustomerMapper CUSTOMER_MAPPER = Mappers.getMapper(CustomerMapper.class);
    CustomerResponse customerToCustomerResponse(Customer customer);
    Customer customerRequestToCustomer(CustomerRequest customerRequest);
}
