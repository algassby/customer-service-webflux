package com.barry.customerservice.dto.response;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CustomerResponse {

    private String customerId;
    private String fullName;
    private String email;
    private Long age;
    private String address;
    private String phoneNumber;
}
