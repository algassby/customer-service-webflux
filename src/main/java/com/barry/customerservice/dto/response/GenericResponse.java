package com.barry.customerservice.dto.response;

import org.springframework.http.HttpStatus;

public record GenericResponse (CustomerResponse customerResponse, HttpStatus httpStatus, String message){}

