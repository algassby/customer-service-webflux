package com.barry.customerservice.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CustomerRequest {
    private String fullName;
    private String email;
    private Long age;
    private String address;
    private String phoneNumber;
}
