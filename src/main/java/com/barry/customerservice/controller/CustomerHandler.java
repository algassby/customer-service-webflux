package com.barry.customerservice.controller;

import com.barry.customerservice.dto.request.CustomerRequest;
import com.barry.customerservice.dto.response.CustomerResponse;
import com.barry.customerservice.service.CustomerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.net.URI;

import static org.springframework.web.reactive.function.BodyInserters.fromValue;
import static org.springframework.web.reactive.function.server.ServerResponse.*;


@Component
@RequiredArgsConstructor
@Slf4j
public class CustomerHandler {
    private final CustomerService customerService;

    public Mono<ServerResponse> findAllCustomers(ServerRequest  serverRequest){

        return customerService.findAll()
                .collectList()
                .flatMap(customers->{
                    if(customers.isEmpty()){
                        return noContent().build();
                    }
                return ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(fromValue(customers));

                });

    }

    public Mono<ServerResponse> getCustomer(ServerRequest  serverRequest){

        String customerId = serverRequest.pathVariable("customerId");
        return customerService.findById(customerId)
                .flatMap(customer-> ok().contentType(MediaType.APPLICATION_JSON)
                        .bodyValue(customer))
                .switchIfEmpty(ServerResponse.notFound().build()).log();

    }

    public Mono<ServerResponse> save(ServerRequest serverRequest){

        Mono<CustomerRequest> customerRequestMono = serverRequest.bodyToMono(CustomerRequest.class);
        return customerRequestMono
                .flatMap(customerService::save)
                .doOnSuccess(customerResponse -> log.info(String.join(" ", "Created user with id",
                        customerResponse.getCustomerId(), "successfuly")))
                .doOnError(error -> log.error(error.getMessage()))
                .flatMap(customerResponse -> created(getUri(customerResponse)).bodyValue(customerResponse));

    }

    public Mono<ServerResponse> update(ServerRequest serverRequest){
        String customerId = serverRequest.pathVariable("customerId");
        Mono<CustomerRequest> customerRequestMono = serverRequest.bodyToMono(CustomerRequest.class);
        return customerRequestMono
                .flatMap(customerRequest ->
                    customerService.update(customerRequest, customerId))
                .doOnSuccess(customerResponse -> log.info(String.join(" ", "Created user with id",
                        customerResponse.getCustomerId(), "successfuly")))
                .doOnError(error -> log.error(error.getMessage()))
                .flatMap(customerResponse -> created(getUri(customerResponse)).bodyValue(customerResponse))
                .switchIfEmpty(Mono.empty());

    }
    public Mono<ServerResponse> deleteCustomer(ServerRequest serverRequest){
        String customerId = serverRequest.pathVariable("customerId");
        return
                customerService.delete(customerId)
                        .flatMap(genericResponse ->  ok().bodyValue(genericResponse));


    }



    private static URI getUri(CustomerResponse customerResponse){
        return UriComponentsBuilder
                .fromPath("/{customerId}")
                .buildAndExpand(customerResponse).toUri();
    }

}
