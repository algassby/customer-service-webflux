package com.barry.customerservice.controller.router;

import com.barry.customerservice.controller.CustomerHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.*;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Component
public class CustomerRouter {
    private static final String CUSTOMERS_API = "/api/v1/customers";
    private static final String CUSTOMERS_ID = "/{customerId}";

    @Bean
    RouterFunction<ServerResponse> responseRouterFunction (CustomerHandler customerHandler){
        return route(GET(CUSTOMERS_API), customerHandler::findAllCustomers )
                .andRoute(GET(CUSTOMERS_API + CUSTOMERS_ID), customerHandler::getCustomer)
                .andRoute(POST(CUSTOMERS_API + "/save").and(accept(MediaType.APPLICATION_JSON)),
                        customerHandler::save)
                .andRoute(PUT(CUSTOMERS_API + CUSTOMERS_ID + "/update").and(accept(MediaType.APPLICATION_JSON)),
                        customerHandler::update)
                .andRoute(DELETE(CUSTOMERS_API + CUSTOMERS_ID + "/delete"), customerHandler::deleteCustomer);
    }

}
