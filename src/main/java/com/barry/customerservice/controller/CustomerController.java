package com.barry.customerservice.controller;

import com.barry.customerservice.dto.request.CustomerRequest;
import com.barry.customerservice.dto.response.CustomerResponse;
import com.barry.customerservice.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;
import reactor.util.function.Tuple2;

import java.time.Duration;
import java.util.stream.Stream;

@RestController
@RequestMapping("/api/v1/customers-stream")
@RequiredArgsConstructor
public class CustomerController {
    private final CustomerService customerService;
    private final Sinks.Many<CustomerResponse> customerResponseMany;
    private final Flux<CustomerResponse> customerResponseFlux;

    @GetMapping(produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<CustomerResponse> getCustomerAsStream(@RequestParam(name = "value", defaultValue =  "0",required = false  ) String value){
        return customerService.findAll()
                .flatMap(customerResponse -> Flux
                        .zip(Flux.interval(Duration.ofSeconds(5), Duration.ofSeconds(Long.parseLong(value))),
                                Flux.fromStream(Stream.generate(()-> customerResponse)))
                .map(Tuple2::getT2));

    }

    @PostMapping(value = "/save")
    public Mono<CustomerResponse> save(@RequestBody CustomerRequest customerRequest, @RequestParam(name = "value", defaultValue =  "0",required = false  ) String value){
        return customerService.save(customerRequest)
                .doOnSuccess(customerResponseMany::tryEmitNext);

    }
    @GetMapping(value = "/new", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<CustomerResponse> newUpdateStream(){
        return customerResponseFlux;
    }


}
