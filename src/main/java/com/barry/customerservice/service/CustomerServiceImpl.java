package com.barry.customerservice.service;

import com.barry.customerservice.dto.request.CustomerRequest;
import com.barry.customerservice.dto.response.CustomerResponse;
import com.barry.customerservice.dto.response.GenericResponse;
import com.barry.customerservice.exception.GenericException;
import com.barry.customerservice.model.Customer;
import com.barry.customerservice.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static com.barry.customerservice.mapper.CustomerMapper.CUSTOMER_MAPPER;

@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService{

    private final CustomerRepository customerRepository;

    @Override
    public Flux<CustomerResponse> findAll() {
         return customerRepository.findAll()
                 .map(CUSTOMER_MAPPER::customerToCustomerResponse);
    }

    @Override
    public Mono<CustomerResponse> findById(String customerId) {
        return customerRepository.findById(customerId)
                .map(CUSTOMER_MAPPER::customerToCustomerResponse).log();
    }

    @Override
    public Mono<CustomerResponse> findByEmail(String email) {

        return customerRepository.findById(email)
                .map(CUSTOMER_MAPPER::customerToCustomerResponse);
    }

    @Override
    public Mono<Boolean> existsByEmail(String email) {
        return customerRepository.existsByEmail(email);
    }

    @Override
    public Mono<CustomerResponse> save(CustomerRequest customerRequest) {
        return existsByEmail(customerRequest.getEmail()).
                flatMap(exitsBy->{
                    if(Boolean.TRUE.equals(exitsBy)){
                        return Mono.error(new GenericException("The customer with email" + customerRequest.getEmail() + "already exists"));
                    }
                   return Mono.just(customerRequest)
                            .flatMap(customRequest -> {
                                Customer customer = CUSTOMER_MAPPER.customerRequestToCustomer(customerRequest);
                                return customerRepository.save(customer);
                            })
                           .map(CUSTOMER_MAPPER::customerToCustomerResponse);
                });
    }

    @Override
    public Mono<CustomerResponse> update(CustomerRequest customerRequest, String customerId) {
        return customerRepository.findById(customerId)
                .flatMap(customer->{
                    Customer customerUpdated = CUSTOMER_MAPPER.customerRequestToCustomer(customerRequest);
                    customerUpdated.setCustomerId(customer.getCustomerId());
                     return  customerRepository.save(customerUpdated)
                             .map(CUSTOMER_MAPPER::customerToCustomerResponse);

                })
                .switchIfEmpty(Mono.empty());
    }

    @Override
    public Mono<GenericResponse> delete(String customerId) {
        return customerRepository.findById(customerId)

                .flatMap(customer-> customerRepository.deleteById(customerId).
                         then(Mono.just(new GenericResponse(
                                 null, HttpStatus.OK , "user deleted successfuly."))))
                .switchIfEmpty(Mono.just(new GenericResponse(
                        null, HttpStatus.NOT_FOUND , "user with id "+ customerId +" not found.")));
    }
}
