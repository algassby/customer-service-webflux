package com.barry.customerservice.service;

import com.barry.customerservice.dto.request.CustomerRequest;
import com.barry.customerservice.dto.response.CustomerResponse;
import com.barry.customerservice.dto.response.GenericResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface CustomerService {

    Flux<CustomerResponse> findAll();
    Mono<CustomerResponse> findById(String customerId);
    Mono<CustomerResponse> findByEmail(String email);
    Mono<Boolean> existsByEmail(String email);
    Mono<CustomerResponse> save(CustomerRequest customerRequest);
    Mono<CustomerResponse> update(CustomerRequest customerRequest, String customerId);
    Mono<GenericResponse> delete(String customerId);

}
