package com.barry.customerservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author algas
 *
 */

@Document("customers")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Customer {

	@Id
	private String customerId;
	private String fullName;
	private String email;
	private Long age;
	private String address;
	private String phoneNumber;

	
}
