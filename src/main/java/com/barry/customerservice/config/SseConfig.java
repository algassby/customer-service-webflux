package com.barry.customerservice.config;

import com.barry.customerservice.dto.response.CustomerResponse;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

@Component
public class SseConfig {

    @Bean
    public Sinks.Many<CustomerResponse> getCustomerResponseMany(){
        return Sinks.many().replay().limit(1);
    }

    @Bean

    public Flux<CustomerResponse> getCustomerResponseFlux(Sinks.Many<CustomerResponse> sinks){
        return sinks.asFlux();
    }

}
